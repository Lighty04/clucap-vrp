#!/bin/bash

for f in `find dataset -name "*.txt"`; do 
    echo $f
    timeout 10s python3 calculate_vrp.py $f $1
done
