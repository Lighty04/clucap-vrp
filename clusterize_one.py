import data_converter
import sys
import os
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from helper import *

if(len(sys.argv) != 2):
    print("Missing file name!!!")
    exit(1)

tsp_root = "dataset/"
tsp_dir = tsp_root + os.path.basename(sys.argv[1].split(".")[0]) + "/"
if os.path.exists(tsp_dir):
    print("This data alreday exists!!!")
    exit(1)

number_of_cities, vehicle_capacity, number_of_vehicles, depot, data = data_converter.read_data(sys.argv[1])
vehicle_capacity = int(vehicle_capacity)
print("Number of cities: ", number_of_cities)
print("Vehicle capacity: ", vehicle_capacity)
print("Number of vehicles: ", number_of_vehicles)
print("Depot: ", depot)
countdown = 10
while(True):
    centers = data_converter.seed_selection(data, number_of_vehicles, (len(data)//3))
    clustered_data = data_converter.cluster(centers, data)
    if(countdown < 0):
        print("Cannot find valid cluster data!")
        exit(1)
    if(clustered_data == None or not check_capacities(clustered_data, vehicle_capacity)):
        vehicle_capacity = int(vehicle_capacity*1.1)
        countdown -= 1
        continue
    break

os.makedirs(tsp_dir)

file_stream = open(tsp_dir + os.path.basename(sys.argv[1]).split(".")[0] + "_clustered.txt", "w+")
index = 0
file_stream.write("Number of vehicles: " + str(number_of_vehicles) + "\n")
file_stream.write("Vehicle capacity: " + str(vehicle_capacity) + "\n")
file_stream.write("Depot: " + str(depot[0]) + "," + str(depot[1]) + "\n")
for key in clustered_data.keys():
    file_stream.write(str(index) + " (" + str(sum(list(map(lambda x: x[2], clustered_data[key])))) + ")" + "\n")
    for city in clustered_data[key]:
        file_stream.write("\t" + str(city[0]) + "," + str(city[1]) + "," + str(city[2]) + "\n")
    index += 1
file_stream.close()

