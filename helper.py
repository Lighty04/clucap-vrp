import sys
import data_converter
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

def draw_line(p1, p2, color):
    x1, y1 = [p1[0], p2[0]], [p1[1], p2[1]]
    plt.plot(x1, y1, marker='o', color=color)

def draw_lines(paths, solution_map):
    colors_list = list(colors._colors_full_map.values())
    for index in range(len(solution_map)):
        dots = solution_map[index]
        curr_path = paths[index]
        color = colors_list[index]
        for path_index in range(-1, len(paths[index])-1):
            draw_line(dots[curr_path[path_index]], dots[curr_path[path_index+1]], color)

def draw_dots(sol_map, file_name, paths=None):
    index = 0
    for key in sol_map.keys():
        xs = list(map(lambda x: x[0], sol_map[key]))
        ys = list(map(lambda x: x[1], sol_map[key]))
        plt.scatter(xs, ys)
        index += 1
    if(paths != None): draw_lines(paths, sol_map)
    plt.savefig(file_name)

def sort_by_distance(data, depot):
    return sorted(data, key=lambda x: data_converter.distance((x[0], x[1]), depot))

def check_capacities(clustered_data, vehicle_capacity):
    for key in clustered_data.keys():
        cluster_capacity = 0
        for city in clustered_data[key]:
            cluster_capacity += city[2]
        if(cluster_capacity > vehicle_capacity):
            print("At least one cluster has capacity which greater than vehicle capacity (", cluster_capacity, ") for file ", sys.argv[1])
            return False
    return True

def read_clustered_data(file_name):
    clustered_data = {}
    number_of_vehicles = 0
    vehicle_capacity = 0
    depot = None
    with open(file_name) as f:
        number_of_vehicles = int(f.readline().split(":")[1].strip())
        vehicle_capacity = int(f.readline().split(":")[1].strip())
        depot = [int(x) for x in f.readline().split(":")[1].split(",")]


        index = 0
        while(True):
            line = f.readline().strip()
            if(line == ""): break
            if(len(line.split(",")) == 1):
                index += 1
                clustered_data[index] = []
                continue
            else:
                clustered_data[index].append([int(x) for x in line.split(",")])
    return (number_of_vehicles, vehicle_capacity, depot, clustered_data)
