For making clustered data out of regular you can call script:

_bash make_all_data.sh_

And for starting calculation over clustered data you can call script:

_bash calculate_all_data.sh output-folder_

New dataset is in folder _dataset_ with folder made for every instance.

Solutions are in folder _solution/output-folder/dataset_ with folder made for
every instance.



