from random import shuffle
import math

def distance(p1, p2):
    return math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)

def average(p1, points):
    sum = 0
    for p in points:
        sum += distance(p1, p)
    return sum/len(points)

def arg_average(points1, points2, func):
    curr_average = average(points1[0], points2)
    sol = points1[0]
    for p1 in points1:
        calc_average = average(p1, points2)
        if(func(curr_average, calc_average)):
            curr_average = calc_average
            sol = p1
    return sol

def seed_selection(m, t_size, num_s):
    n = m.copy()
    shuffle(n)
    temp_cluster = [n[0]]
    while len(temp_cluster) < t_size:
        shuffle(n)
        if(n[0] not in temp_cluster):
            temp_cluster.append(n[0])

    centers = [arg_average(temp_cluster, temp_cluster, lambda x, y: x > y)]
    for _ in range(1, num_s):
        s = arg_average(n, centers, lambda x, y: x < y)
        n.remove(s)
        centers.append(s)

    return centers

def remove_filled(centers, copy_centers, clustered_data, capacity, customer):
    for c in centers:
        if(sum(list(map(lambda x: x[2], clustered_data[c]))) + customer[2] > capacity):
            copy_centers.remove(c)
    return copy_centers


def select_closest_customer(centers, customers):
    closest_centers = []
    for customer in customers:
        closest_centers.append(arg_average(centers, [customer], lambda x, y: x > y))

    dist = distance(closest_centers[0], customers[0])
    customer = customers[0]
    for index in range(len(closest_centers)):
        if(dist > distance(closest_centers[index], customers[index])):
            dist = distance(closest_centers[index], customers[index])
            customer = customers[index]

    return customer

def cluster(centers, customers):
    clustered_data = {}
    for center in centers:
        clustered_data[center] = [center]

    copy_customers = customers.copy()
    while(copy_customers != []):
        copy_centers = centers.copy()
        customer = select_closest_customer(copy_centers, copy_customers)
        if(customer in centers):
            copy_customers.remove(customer)
            continue
        if(copy_centers == []): return None
        center = arg_average(copy_centers, [customer], lambda x, y: x > y)
        clustered_data[center].append(customer)
        copy_customers.remove(customer)

    return clustered_data

def cluster_watching_capacity(centers, customers, vehicle_capacity):
    clustered_data = {}
    for center in centers:
        clustered_data[center] = [center]

    for customer in customers:
        if(customer in clustered_data): continue
        centers_copy = list(centers)
        while(True):
            if(centers_copy == []): return None
            center = arg_average(centers_copy, [customer], lambda x, y: x > y)
            if(sum(list(map(lambda x: x[3], clustered_data[center]))) + customer[3] > vehicle_capacity):
                centers_copy.remove(center)
            else:
                break

        clustered_data[center].append(customer)

    return clustered_data

def read_data(file_name):
    file_stream = open(file_name, "r")
    data = []

    _ = file_stream.readline()
    number_of_vehicles = int(file_stream.readline().split(" ")[8][:-1])
    _ = file_stream.readline()
    number_of_cities = int(file_stream.readline().split(" ")[-1])
    _ = file_stream.readline()
    vehicle_capacity = int(file_stream.readline().split(" ")[-1])
    _ = file_stream.readline()


    s_line = file_stream.readline().strip().split()
    depot = ((int(s_line[1]), int(s_line[2])))
    for _ in range(number_of_cities - 1):
        line = file_stream.readline().strip()
        s_line = line.split()
        data.append((int(s_line[1]), int(s_line[2])))

    file_stream.readline()

    index = 0
    res_data = []
    file_stream.readline()
    for _ in range(number_of_cities - 1):
        line = file_stream.readline().strip()
        res_data.append((data[index][0], data[index][1], int(line.split(" ")[1])))
        index += 1

    return (number_of_cities, vehicle_capacity, number_of_vehicles, depot, res_data)




