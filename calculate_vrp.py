import data_converter
import sys
import os
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from helper import *
from subprocess import Popen, PIPE, STDOUT

if(len(sys.argv) != 3):
    print("Missing file name!!!")
    print("Missing number of soultion folder!!!")
    exit(1)

number_of_vehicles, vehicle_capacity, depot, clustered_data = read_clustered_data(sys.argv[1])

countdown = 10
while(True):
    if(countdown <= 0):
        print("Cannot calculate solution for this instance!!!")
        exit(1)
    countdown -= 1

    avg_centers = []
    index = 0
    for key in clustered_data.keys():
        avg_x = sum(list(map(lambda x: x[0], clustered_data[key])))/len(clustered_data[key])
        avg_y = sum(list(map(lambda x: x[1], clustered_data[key])))/len(clustered_data[key])
        needed_capacity = sum(list(map(lambda x: x[2], clustered_data[key])))
        avg_centers.append((avg_x, avg_y, index, needed_capacity))
        index += 1

    center_of_centers = data_converter.seed_selection(avg_centers, number_of_vehicles, number_of_vehicles)
    centers_clustered_data = data_converter.cluster_watching_capacity(center_of_centers, avg_centers, vehicle_capacity)
    if(centers_clustered_data == None): continue

    solution_map = {}
    index = 0
    for center in centers_clustered_data.keys():
        solution_map[index] = []
        for avg_center in centers_clustered_data[center]:
            solution_map[index].extend(clustered_data[list(clustered_data.keys())[avg_center[2]]])
        index += 1

    if(check_capacities(solution_map, vehicle_capacity)):
        for key in solution_map.keys():
            print(key)
            for city in solution_map[key]:
                print("\t", city)
        break
    print("REPEAT")

tsp_dir = "solution/" + sys.argv[2] + "/" + os.path.dirname(sys.argv[1].split(".")[0]) + "/"
os.makedirs(tsp_dir)
file_basename = os.path.basename(sys.argv[1].split(".")[0])
draw_dots(clustered_data, tsp_dir + "clustered_image.png")

all_distance = 0
paths = []
with open(tsp_dir + "solution.txt", "w+") as f:
    for index in solution_map.keys():
        file_stream = open(tsp_dir + "vehicle_" + "{:02d}".format(index) + ".tsp", "w+")
        file_stream.write(str(depot[0]) + "," + str(depot[1]) + "\n")
        for city in solution_map[index]:
            file_stream.write(str(city[0]) + "," + str(city[1]) + "\n")
        file_stream.close()

        p = Popen(['java', '-jar', 'KohonenTsp.jar', tsp_dir + 'vehicle_' + "{:02d}".format(index) + '.tsp'], stdout=PIPE, stderr=STDOUT)
        path = [(int(x) if x != "" else None) for x in p.stdout.readline().decode("ascii").split(":")[1].strip()[1:-1].split(",")]
        path = list(filter(lambda x: x != None, path))
        line = p.stdout.readline()
        distance = float(line.decode("ascii").split(":")[1].strip())
        f.write("Vehicle " + str(index) + " path: " + str(path) + ", distance: " + str(distance) + ", capacity: " + str(sum(list(map(lambda x: x[2], solution_map[index])))) + "/" + str(vehicle_capacity) + "\n")
        all_distance += distance
        paths.append(path)
    f.write("Distance for all vehicles: " + str(all_distance) + "\n")

for key in solution_map.keys():
    solution_map[key] = [[depot[0], depot[1], 0]] + solution_map[key]

draw_dots(solution_map, tsp_dir + "solution_image.png", paths)

